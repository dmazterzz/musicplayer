<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'track_id', 'track_name', 'album_id', 'running_time', 'track_file_name'
    ];

    public function album(){
        return $this->belongsTo(Album::class);
    }
}
