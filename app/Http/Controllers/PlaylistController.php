<?php

namespace App\Http\Controllers;

use App\Playlist;
use App\LinkPlaylistTrack;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class PlaylistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $playlist = Playlist::create([
            'playlist_name' => $request->playlist_name
        ]); 

        $AllPlaylists = $this->_getAllPlaylists();
        if($AllPlaylists){
            return response()->json([
                'playlist' => $AllPlaylists,
                'message' => 'Playlist created!'
            ],200);
        }else{
            return response()->json([
                'error' => 'Nothing Found'
            ],404);
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Playlist  $playlist
     * @return \Illuminate\Http\Response
     */
    public function show(Playlist $playlist)
    {
       $AllPlaylists = $this->_getAllPlaylists();

        if($AllPlaylists){
            return response()->json([
                'playlist' => $AllPlaylists
            ],200);
        }else{
            return response()->json([
                'error' => 'Nothing Found'
            ],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Playlist  $playlist
     * @return \Illuminate\Http\Response
     */
    public function edit(Playlist $playlist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Playlist  $playlist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Playlist $playlist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Playlist  $playlist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Playlist $playlist)
    {
        //
    }

    public function addTracksToPlaylist(Request $request, Playlist $playlist){
        $playlistID = $request->playlist_id;
        $addTrackAry = $request->tracks;
    }


    private function _getAllPlaylists()
    {
      return $AllPlaylists = DB::select('select * from playlists');
   
    }
}
