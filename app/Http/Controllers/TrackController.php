<?php
/**
 * Class LibraryController
 * Handles gettting tracks from the database
 * 
 *  */ 
namespace App\Http\Controllers;

use App\Track;
use App\Artist;
use App\Album; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class TrackController extends Controller
{
    private $metaData; //class variable to store the metadata from the file

    public function getMusicURL(Request $request)
    {
        $filename = $request->track_file_name;

        $fLocation = Storage::url('/storage/upload/files/audio/'.$filename);

        if($ArtistTracks){
            return response()->json([
                'tracks' => $ArtistTracks
            ],200);
        }else{
            return response()->json([
                'error' => 'Nothing Found'
            ],404);
        }
                
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
   
        $musicFile = $request->file('audio');
        if($this->_verifyFileType($musicFile->getClientMimeType())){
            //Store file first 
            $path = Storage::putFileAs(
                'music', $musicFile, 'public'
            );

            
            if(is_array($request->file('audio'))){// handle multiple files 
                $audios=array();
                foreach($request->file('audio') as $file) {
                    $uniqueid=uniqid();
                    $original_name=$file->getClientOriginalName();
                    $size=$file->getSize();
                    $extension=$file->getClientOriginalExtension();
                    $filename=Carbon::now()->format('Ymd').'_'.$uniqueid.'.'.$extension;
                    $audiopath=url('/storage/upload/files/audio/'.$filename);
                    $path=$file->storeAs('/upload/files/audio',$filename);
                    array_push($audios,$audiopath);
                }
                $all_audios=implode(",",$audios);

            }else if($request->hasFile('audio')){ //handle single file
                $uniqueid=uniqid();
                $original_name=$request->file('audio')->getClientOriginalName();
                $size=$request->file('audio')->getSize();
                $extension=$request->file('audio')->getClientOriginalExtension();
                $filename=$original_name;
                $audiopath=url('/storage/upload/files/audio/'.$filename);
                $path=$musicFile->storeAs('files/audio',$filename);
                $all_audios=$audiopath;
            }

            //Create a tempoary file locally then get the metadata
            $contents = $exists = Storage::disk('public')->get($path);
            $tmpfname = tempnam(sys_get_temp_dir(), "FOO");
            file_put_contents($tmpfname, $contents);
            $getID3 = new \getID3; //library to read mp3 data
            $tempAry = $getID3->analyze($tmpfname);

            $this->metaData = $tempAry['id3v1'];
            $this->metaData['playtime_string'] = $tempAry['playtime_string'];

            //Check if artist already exists
            $artistInDB = DB::select('select id from artists where artist_name = ?',[$this->metaData['artist']]);
        
            if($artistInDB){
                $albumFromArtist = DB::select('select id from albums where album_name = :album_name and id = :artist_id ', 
                                    ['album_name' => $this->metaData['album'],
                                     'artist_id' => $artistInDB[0]->id]);
                    
                if($albumFromArtist){ //album was found and artist was found
                    $this->_addNewTrack($this->metaData['title'],$albumFromArtist[0]->id, 
                                        $this->metaData['playtime_string'],$filename);
                }else{
                    $this->_storeNew($filename, 2,$artistInDB[0]->id); //artist was found album was not
                }
            }else{
                $this->_storeNew($filename, 1); //artist doesn't exist
            }
   
            //return a list of all tracks include the newly added track
              $AllTracks = DB::select('select t.id, track_name,t.track_file_name, art.artist_name, alb.album_name  
                                        from tracks t
                                        INNER JOIN albums alb ON alb.id = t.album_id
                                        INNER JOIN artists art on art.id = alb.artist_id');

                return response()->json([
                    'message' => 'Track Added Successfully. Please refresh browser',
                    'tracks' => $AllTracks
                ]);

        }else{
            //File was not valid
            return response()->json([
                'message' => 'Track Format is invalid',
                'format' => $musicFile->getClientMimeType()
            ]);
        }
         
    }

    //function to check if file is valid
    private function _verifyFileType($ext)
    {
        //use an array can be expanded to support more file types
        $validFileType = array('audio/mpeg', 'audio/mp3');

        if(in_array($ext, $validFileType)){
            return true; //The file is valid
        }

        return false; //the file is invalid 
    }

    /**
     * function addNewArtist
     * @param string $artistName 
     * @return artist ID
     */
    private function _addNewArtist($artistName)
    {
        $artist = Artist::create([
            'artist_name' => $artistName
        ]);

        return $artist->id;
    }
    
        /**
     * function _addNewAlbum
     * @param string $albumName 
     * @return album ID
     */
    private function _addNewAlbum($albumName, $artist_id)
    {
        $album = Album::create([
            'album_name' => $albumName,
            'artist_id' => $artist_id
        ]);

        return $album->id;
    }

    /**
     * function _addNewTrack
     * @param string $albumName 
     * 
     */
    private function _addNewTrack($trackName, $album_id, $running_time,$track_file_name)
    {
        $track = Track::create([
            'track_name' => $trackName,
            'album_id' => $album_id,
            'running_time' => $running_time,
            'track_file_name' => $track_file_name,
        ]);

        
    }

    private function _storeNew($filename, $level, $artist_id = null){
        
        switch($level){
            case 1: //Artist not found
                $newArtistId = $this->_addNewArtist($this->metaData['artist']);
                $newAlbumId = $this->_addNewAlbum($this->metaData['album'],$newArtistId);
                $runnigTime = $this->metaData['playtime_string'];
                $newTrackId = $this->_addNewTrack($this->metaData['title'],$newAlbumId,$runnigTime,$filename);

                break;
            case 2: //album not found
                $newAlbumId = $this->_addNewAlbum($this->metaData['album'],$artist_id);
                $runnigTime = $this->metaData['playtime_string'];
                $newTrackId = $this->_addNewTrack($this->metaData['title'],$newAlbumId,$runnigTime,$filename);
                break;
            default://Do nothing
                break;
        }
       
    }
}
