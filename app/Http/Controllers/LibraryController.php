<?php

/**
 * Class LibraryController
 * Handles gettting tracks from the database
 * 
 *  */ 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Track;


class LibraryController extends Controller
{
    //Get all tracks along with the album and artist
    public function show(Track $track)
    {
        $AllTracks = DB::select('select t.id, track_name,t.track_file_name, art.artist_name, alb.album_name, t.running_time
                                from tracks t
                                INNER JOIN albums alb ON alb.id = t.album_id
                                INNER JOIN artists art on art.id = alb.artist_id');
        
        if($AllTracks){
            return response()->json([
                'tracks' => $AllTracks
            ],200);
        }else{
            return response()->json([
                'error' => 'Nothing Found'
            ],200);
        }
        
    }

    //search for a track by using filtering the artist table
    public function searchByArtist(Request $request, Track $track)
    {
        
        $ArtistTracks = DB::select("select t.id, track_name,t.track_file_name, art.artist_name, alb.album_name, t.running_time 
                                    from tracks t 
                                    INNER JOIN albums alb ON alb.id = t.album_id 
                                    INNER JOIN artists art on art.id = alb.artist_id 
                                    WHERE lower(art.artist_name) like :artist_name",
                                    ['artist_name' => '%'.$request->artist_name.'%']);

        if($ArtistTracks){
            return response()->json([
                'tracks' => $ArtistTracks
            ],200);
        }else{
            return response()->json([
                'error' => 'Nothing Found'
            ],200);
        }
                                                              
    }

    //search for a track by filtering the track table
    public function searchByTrack(Request $request, Track $track)
    {
        
        $Tracks = DB::select("select t.id, track_name,t.track_file_name, art.artist_name, alb.album_name, t.running_time 
                                    from tracks t 
                                    INNER JOIN albums alb ON alb.id = t.album_id 
                                    INNER JOIN artists art on art.id = alb.artist_id 
                                    WHERE lower(t.track_name) like :track_name",
                                    ['track_name' => $request->track_name.'%']);

        if($Tracks){
            return response()->json([
                'tracks' => $Tracks
            ],200);
        }else{
            return response()->json([
                'error' => 'Nothing Found'
            ],200);
        }
                                                              
    }
}
