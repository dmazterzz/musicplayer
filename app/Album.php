<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'album_id', 'album_name', 'artist_id'
    ];

    public function artist(){
        return $this->belongsTo(Artist::class, 'artist_id');
    }
}
