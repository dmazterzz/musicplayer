<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkPlaylistTrack extends Model
{
    public $timestamps = false;
    
    protected $fillable = [
        'link_playlist_tracks','playlist_id','track_id'
    ];

    public function playlist(){
        return $this->belongsTo(Artist::class, 'playlist_id');
    }

    public function track(){
        return $this->belongsTo(Artist::class, 'track_id');
    }
}
