<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForignKeyToLink extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('link_playlist_tracks', function (Blueprint $table) {
            $table->unsignedBigInteger('playlist_id');
            $table->foreign('playlist_id')
            ->references('id')->on('playlists')
            ->onDelete('cascade');
            $table->unsignedBigInteger('track_id');

            $table->foreign('track_id')
            ->references('id')->on('tracks')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('link_playlist_tracks', function (Blueprint $table) {
            $table->dropColumn(['playlist_id', 'track_id']);
        });
    }
}
