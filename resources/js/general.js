/*
File to be used for any general function
or global variable
*/

//var musicFileList = null;
export const INACTIVE_USER_TIME_THRESHOLD = 30000;
export const USER_ACTIVITY_THROTTLER_TIME = 10000;

//Change tabs on click
$(document).ready(function() {
    $('#tabs li').on('click', function() {
      var tab = $(this).data('tab');
  
      $('#tabs li').removeClass('is-active');
      $(this).addClass('is-active');
  
      $('#tab-content p').removeClass('is-active');
      $('p[data-content="' + tab + '"]').addClass('is-active');
    });
  });



