require('./bootstrap');
require('./general')

import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import 'bulma/css/bulma.css'
import columnSortable from 'vue-column-sortable'

Vue.use(columnSortable)

import MainPage from './pages/MainPage'

Vue.prototype.$musicFileList //Global variable to be used to keep track of the file list

//set up the vue routes 
const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: MainPage
        }
    ]
})

Vue.component('index', MainPage)

//init App
const app = new Vue({
    el: '#app',
    router,
});